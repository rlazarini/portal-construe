
httpVueLoader.langProcessor.scss = function (scssText) {
    return new Promise(function(resolve, reject) {
        var sass = new Sass();
        sass.compile(scssText, function (result) {
            if ( result.status === 0 )
                resolve(result.text)
            else
                reject(result)
        });
    });
}

var App = httpVueLoader('templates/App.vue')
,   Second = httpVueLoader('templates/nd-component.vue')
,   First = httpVueLoader('templates/st-component.vue')
,   router = new VueRouter({
    mode: 'hash',
    routes:[
        {
            path: (!!location.pathname.split('/')[1] ? '/' + location.pathname.split('/')[1] : '') + '/',
            component: First
        },
        {
            path:(!!location.pathname.split('/')[1] ? '/' + location.pathname.split('/')[1] : '') + '/segundo',
            component: Second
        }
    ]
});

new Vue({
  el: '#app',
  router,
  components: { App }
})



